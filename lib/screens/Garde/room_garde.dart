import 'file:///C:/Users/dell/AndroidStudioProjects/bifadden/lib/screens/bricollage/bricollage_body.dart';
import 'package:bifadden/screens/Garde/garde_body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class roomgarde extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: "oswald"),
      home: RoomGarde(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class RoomGarde extends StatefulWidget {
  RoomGarde({Key key}) : super(key: key);

  @override
  _RoomGardeState createState() => _RoomGardeState();
}

class _RoomGardeState extends State<RoomGarde> {
  int _currentindex = 0;
  List tabs = [
    gardebody(),
    // Container(
    //   color: Colors.greenAccent,
    // ),
    // Container(
    //   color: Colors.pink,
    // ),
    // Container(
    //   color: Colors.yellow,
    // ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          body: tabs[_currentindex],
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _currentindex,
            elevation: 5,
            backgroundColor: Colors.white54,
            selectedItemColor: Colors.red[800],
            type: BottomNavigationBarType.fixed,
            selectedIconTheme: IconThemeData(size: 30),
            selectedFontSize: 15,
            items: [
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                ),
                title: Text("Accueil"),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.location_city,
                ),
                title: Text("Ville"),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.settings,
                ),
                title: Text("Parametres"),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.star,
                ),
                title: Text("Favories"),
              ),
            ],
            onTap: (index) {
              setState(() {
                _currentindex = index;
              });
            },
          ),
        ),
      ),
    );
  }
}

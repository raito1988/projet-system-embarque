import 'coiffure_body.dart';
import 'file:///C:/Users/dell/AndroidStudioProjects/bifadden/lib/screens/bricollage/bricollage_body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class roomcoiffure extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: "oswald"),
      home: RoomCoiffure(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class RoomCoiffure extends StatefulWidget {
  RoomCoiffure({Key key}) : super(key: key);

  @override
  _RoomCoiffureState createState() => _RoomCoiffureState();
}

class _RoomCoiffureState extends State<RoomCoiffure> {
  int _currentindex = 0;
  List tabs = [
    coiffbody(),
    // Container(
    //   color: Colors.greenAccent,
    // ),
    // Container(
    //   color: Colors.pink,
    // ),
    // Container(
    //   color: Colors.yellow,
    // ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          body: tabs[_currentindex],
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _currentindex,
            elevation: 5,
            backgroundColor: Colors.white54,
            selectedItemColor: Colors.red[800],
            type: BottomNavigationBarType.fixed,
            selectedIconTheme: IconThemeData(size: 30),
            selectedFontSize: 15,
            items: [
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                ),
                title: Text("Accueil"),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.map_rounded,
                ),
                title: Text("Ville"),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.whatshot,
                ),
                title: Text("Certifié"),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.account_box_rounded,
                ),
                title: Text("Poster"),
              ),
            ],
            onTap: (index) {
              setState(() {
                _currentindex = index;
              });
            },
          ),
        ),
      ),
    );
  }
}

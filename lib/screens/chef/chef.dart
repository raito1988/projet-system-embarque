import 'package:bifadden/screens/chef/chef_body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class chefroom extends StatelessWidget {
  const chefroom({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: "oswald"),
      home: ChefService(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ChefService extends StatefulWidget {
  ChefService({Key key}) : super(key: key);

  @override
  _ChefServiceState createState() => _ChefServiceState();
}

class _ChefServiceState extends State<ChefService> {
  int _currentindex = 0;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List tabs = [
      chefbody(),
      Container(
        color: Colors.greenAccent,
      ),
      Container(
        color: Colors.pink,
      ),
      Container(
        color: Colors.yellow,
      ),
    ];
    return SafeArea(
      child: Scaffold(
        body: tabs[_currentindex],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentindex,
          elevation: 5,
          backgroundColor: Colors.white54,
          selectedItemColor: Colors.red[800],
          type: BottomNavigationBarType.fixed,
          selectedIconTheme: IconThemeData(size: 30),
          selectedFontSize: 15,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              title: Text("Accueil"),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.map_rounded,
              ),
              title: Text("Ville"),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.whatshot,
              ),
              title: Text("Certifié"),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.account_box_rounded,
              ),
              title: Text("Poster"),
            ),
          ],
          onTap: (index) {
            setState(() {
              _currentindex = index;
            });
          },
        ),
      ),
    );
  }
}

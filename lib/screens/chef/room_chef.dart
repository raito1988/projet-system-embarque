import 'chef_body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class roomchef extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: "oswald"),
      home: RoomChef(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class RoomChef extends StatefulWidget {
  RoomChef({Key key}) : super(key: key);

  @override
  _RoomChefState createState() => _RoomChefState();
}

class _RoomChefState extends State<RoomChef> {
  int _currentindex = 0;
  List tabs = [
    chefbody(),
    // Container(
    //   color: Colors.greenAccent,
    // ),
    // Container(
    //   color: Colors.pink,
    // ),
    // Container(
    //   color: Colors.yellow,
    // ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          body: tabs[_currentindex],
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _currentindex,
            elevation: 5,
            backgroundColor: Colors.white54,
            selectedItemColor: Colors.red[800],
            type: BottomNavigationBarType.fixed,
            selectedIconTheme: IconThemeData(size: 30),
            selectedFontSize: 15,
            items: [
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                ),
                title: Text("Accueil"),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.location_city,
                ),
                title: Text("Ville"),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.settings,
                ),
                title: Text("Parametres"),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.star,
                ),
                title: Text("Favories"),
              ),
            ],
            onTap: (index) {
              setState(() {
                _currentindex = index;
              });
            },
          ),
        ),
      ),
    );
  }
}

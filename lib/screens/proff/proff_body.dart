import 'package:bifadden/screens/proff/room_proff.dart';
import 'package:bifadden/widgets/proff_page_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../main.dart';
import '../Room_cleaning.dart';


class proffbody extends StatefulWidget {
  proffbody({Key key}) : super(key: key);

  @override
  _proffbodyState createState() => _proffbodyState();
}

class _proffbodyState extends State<proffbody> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 12,
              top: 20,
            ),
            child: Align(
              alignment: Alignment.topLeft,
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyHomePage(),
                    ),
                  );
                },
                child: CircleAvatar(
                  backgroundColor: Colors.blue[100],
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 45,
                    top: 10,
                    right: 10,
                    bottom: 20,
                  ),
                  child: Text(
                    "Type de Professeur",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 45, top: 10, right: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Proffcard(
                        name: "Arabe",
                        imgpath: "prof arabe.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                      Proffcard(
                        name: "Francais",
                        imgpath: "prof arabe.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 45, top: 30, right: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Proffcard(
                        name: "Anglais",
                        imgpath: "prof arabe.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                      Proffcard(
                        name: "Math",
                        imgpath: "professeur math.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: 45,
                    top: 30,
                    right: 40,
                    bottom: 40,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Proffcard(
                        name: "Physique",
                        imgpath: "professeur math.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 45,
                    right: 10,
                    bottom: 20,
                  ),
                  child: Text(
                    "Offreurs",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 45,
                    bottom: 20,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      freelancers(
                        imgpath: "f1.jpeg",
                        ontap: () {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(milliseconds: 500),
                              content: Text("f1 pressed"),
                            ),
                          );
                        },
                      ),
                      freelancers(
                        imgpath: "f2.jpeg",
                        ontap: () {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(milliseconds: 500),
                              content: Text("f2 pressed"),
                            ),
                          );
                        },
                      ),
                      freelancers(
                        imgpath: "f3.jpeg",
                        ontap: () {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(milliseconds: 500),
                              content: Text("f3 pressed"),
                            ),
                          );
                        },
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomproff(),
                            ),
                          );
                        },
                        child: Container(
                          height: size.height * 0.12,
                          width: size.width * 0.20,
                          decoration: BoxDecoration(
                            color: Colors.black45.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(child: Text("+253..")),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

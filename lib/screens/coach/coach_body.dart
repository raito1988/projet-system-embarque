import 'package:bifadden/screens/coach/room_coach.dart';
import 'package:bifadden/widgets/coach_page_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../main.dart';
import '../Room_cleaning.dart';


class coachbody extends StatefulWidget {
  coachbody({Key key}) : super(key: key);

  @override
  _coachbodyState createState() => _coachbodyState();
}

class _coachbodyState extends State<coachbody> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 12,
              top: 20,
            ),
            child: Align(
              alignment: Alignment.topLeft,
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyHomePage(),
                    ),
                  );
                },
                child: CircleAvatar(
                  backgroundColor: Colors.blue[100],
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 45,
                    top: 10,
                    right: 10,
                    bottom: 20,
                  ),
                  child: Text(
                    "Type de Coaching",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 45, top: 10, right: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Coachcard(
                        name: "Musculation",
                        imgpath: "musculation.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                      Coachcard(
                        name: "Yoga",
                        imgpath: "yoga.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 45, top: 30, right: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Coachcard(
                        name: "Cardio",
                        imgpath: "cardio.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                      Coachcard(
                        name: "Streching",
                        imgpath: "stretching.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: 45,
                    top: 30,
                    right: 40,
                    bottom: 40,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Coachcard(
                        name: "Gymnastique",
                        imgpath: "gymnastique.svg",
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcleaning(),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 45,
                    right: 10,
                    bottom: 20,
                  ),
                  child: Text(
                    "Offreurs",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 45,
                    bottom: 20,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      freelancers(
                        imgpath: "f1.jpeg",
                        ontap: () {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(milliseconds: 500),
                              content: Text("f1 pressed"),
                            ),
                          );
                        },
                      ),
                      freelancers(
                        imgpath: "f2.jpeg",
                        ontap: () {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(milliseconds: 500),
                              content: Text("f2 pressed"),
                            ),
                          );
                        },
                      ),
                      freelancers(
                        imgpath: "f3.jpeg",
                        ontap: () {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(milliseconds: 500),
                              content: Text("f3 pressed"),
                            ),
                          );
                        },
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => roomcoach(),
                            ),
                          );
                        },
                        child: Container(
                          height: size.height * 0.12,
                          width: size.width * 0.20,
                          decoration: BoxDecoration(
                            color: Colors.black45.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(child: Text("+253..")),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

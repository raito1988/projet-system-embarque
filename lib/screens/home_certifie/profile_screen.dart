import 'package:flutter/material.dart';
import 'components/bottom_buttons.dart';
import 'components/categories.dart';
import 'components/custom_app_bar.dart';
import 'components/profiles.dart';

class CertifieScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Column(
            children: [
              CustomAppBar(),
              // Categories(),
              Houses(),
            ],
          ),
          BottomButtons(),
        ],
      ),
    );
  }
}

import 'package:bifadden/models/certifie.dart';

final _house1 = House(
    imageUrl: 'assets/images/coach2.jpg',
    address: 'Skhiti Issam',
    description: 'J\'ai passer plus de 4 ans à entrainer\n et j\'ai le matériel nécessaire pour booster votre physique  ',
    price: 200.00,
    bedRooms: 4,
    bathRooms: 50,
    garages: 4,
    sqFeet: 25,
    time: 20,
    isFav: false,
    moreImagesUrl: [
      'assets/images/coach.jpg',
      'assets/images/coach2.jpg',
      'assets/images/coach5.jfif',
      'assets/images/coach3.jpg',
      'assets/images/coach4.jfif',
      'assets/images/coach5.jfif',
    ]
);
final _house2 = House(
    imageUrl: 'assets/images/bricoleur.jpg',
    address: 'Boughaba Adil',
    description: 'J\'ai passer plus de 12 ans à réparer les coupures fréquentes du courant\n et j\'ai le matériel nécessaire pour remédier à toute pannes possible  ',
    price: 140.00,
    bedRooms: 4,
    bathRooms: 20,
    garages: 5,
    sqFeet: 32,
    time: 30,
    isFav: false,
    moreImagesUrl: [
      'assets/images/bricoleur.jpg',
      'assets/images/bricolleur2.jfif',
      'assets/images/bricolleur3.jfif',
      'assets/images/bricolleur4.jfif',
      'assets/images/bricolleur5.jfif',
      'assets/images/bricoleur.jfif',
    ]
);

final _house3 = House(
    imageUrl: 'assets/images/zahidi.jfif',
    address: 'Zahidi Fatima',
    description: 'J\'ai passer plus de 5 ans à faire du menage\n et j\'ai le matériel nécessaire pour remédier à toute pannes possible  ',
    price: 210.00,
    bedRooms: 5,
    bathRooms:50,
    garages: 4,
    sqFeet: 25,
    time: 30,
    isFav: false,
    moreImagesUrl: [
      'assets/images/zahidi2.jfif',
      'assets/images/zahidi3.jfif',
      'assets/images/zahidi4.jfif',
      'assets/images/zahidi.jfif',
      'assets/images/zahidi3.jfif',
      'assets/images/zahidi2.jfif',
    ]
);


final _house4 = House(
    imageUrl: 'assets/images/choumicha.jfif',
    address: 'Hanafi Soukaina',
    description: 'J\'ai passer plus de 6 ans à réparer les meilleurs repas\n et j\'ai le matériel nécessaire pour remédier à toute pannes possible  ',
    price: 170.00,
    bedRooms: 4,
    bathRooms: 30,
    garages: 6,
    sqFeet: 30,
    time: 30,
    isFav: false,
    moreImagesUrl: [
      'assets/images/choumicha(2).jfif',
      'assets/images/choumicha(3).jfif',
      'assets/images/choumicha(4).jfif',
      'assets/images/choumicha(5).jfif',
      'assets/images/choumicha(6).jfif',
      'assets/images/choumicha(2).jfif',
    ]
);

final _house5 = House(
    imageUrl: 'assets/images/lamita.jpg',
    address: 'Ibouda Lamita',
    description: 'J\'ai passer plus de 3 ans dans l\'enseignement\n et j\'ai le qualites nécessaire pour enseigner  ',
    price: 150.00,
    bedRooms: 3,
    bathRooms: 40,
    garages: 4,
    sqFeet: 27,
    time: 240,
    isFav: false,
    moreImagesUrl: [
      'assets/images/lamita2.jfif',
      'assets/images/lamita3.jfif',
      'assets/images/lamita4.jfif',
      'assets/images/lamita2.jfif',
      'assets/images/lamita3.jfif',
      'assets/images/lamita4.jfif',
    ]
);

final List<House> houseList = [
  _house2,
  _house1,
  _house3,
  _house4,
  _house5,
];


List<String> categoryList = [
  '<\$220.00',
  'For Sale',
  '3-4 bed room',
  'Garages',
  'Modular kitchen'
];

class catagory {
  final String img;
  final String name;
  final String roundimg;
  final String roundimg2;

  catagory({
    this.img,
    this.name,
    this.roundimg,
    this.roundimg2,
  });
  static List<catagory> catagory1 = [
    catagory(
        img: "c1.jpeg",
        name: "Menage",
        roundimg: "f2.jpeg",
        roundimg2: "f3.jpeg"),
    catagory(
        img: "brico.jfif",
        name: "Bricollage",
        roundimg: "f1.jpeg",
        roundimg2: "f3.jpeg"),
    catagory(
        img: "trans.jfif",
        name: "Transport",
        roundimg: "f1.jpeg",
        roundimg2: "f2.jpeg"),
    catagory(
        img: "coiffeur.jpg",
        name: "Coiffure",
        roundimg: "f2.jpeg",
        roundimg2: "f3.jpeg"),
  ];
}

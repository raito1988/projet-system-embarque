class task {
  final String imgpath;
  final String taskname;
  final String bgimgpath;

  task({
    this.imgpath,
    this.taskname,
    this.bgimgpath,
  });

  static List<task> taskk = [
    task(imgpath: "1.svg", taskname: "Coach", bgimgpath: "3.jfif"),
    task(
        imgpath: "2.svg",
        taskname: "Professeur",
        bgimgpath: "4.jfif"),
    task(imgpath: "3.svg", taskname: "Chef", bgimgpath: "1.jfif"),
    task(imgpath: "4.svg", taskname: "Garde", bgimgpath: "2.jfif"),
  ];
}

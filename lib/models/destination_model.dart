import 'activity_model.dart';

class Destination {
  String imageUrl;
  String city;
  String country;
  String description;
  List<Activity> activities;

  Destination({
    this.imageUrl,
    this.city,
    this.country,
    this.description,
    this.activities,
  });
}

List<Activity> activities = [
  Activity(
    imageUrl: 'assets/images/Omar Anlouf.jfif',
    name: 'Omar Anlouf',
    type: 'Sightseeing Tour',
    startTimes: ['9:00 am', '11:00 am'],
    rating: 5,
    price: 30,
  ),
  Activity(
    imageUrl: 'assets/images/Samir.jfif',
    name: 'Samir Ihda',
    type: 'Sightseeing Tour',
    startTimes: ['11:00 pm', '1:00 pm'],
    rating: 4,
    price: 210,
  ),
  Activity(
    imageUrl: 'assets/images/Fayssal.jfif',
    name: 'Fayssal Bourichi',
    type: 'Sightseeing Tour',
    startTimes: ['12:30 pm', '2:00 pm'],
    rating: 3,
    price: 125,
  ),
];

List<Destination> destinations = [
  Destination(
    imageUrl: 'assets/images/maarif.jfif',
    city: 'Maarif',
    country: 'Casablanca',
    description: 'Touvez votre service tout près de chez vous.',
    activities: activities,
  ),
  Destination(
    imageUrl: 'assets/images/agdal.jfif',
    city: 'Agdal',
    country: 'Rabat',
    description: 'Touvez votre service tout près de chez vous.',
    activities: activities,
  ),
  Destination(
    imageUrl: 'assets/images/gueliz.jfif',
    city: 'Gueliz',
    country: 'Marrakech',
    description: 'Touvez votre service tout près de chez vous.',
    activities: activities,
  ),
  Destination(
    imageUrl: 'assets/images/riad.jfif',
    city: 'Hay Riad',
    country: 'Rabat',
    description: 'Touvez votre service tout près de chez vous.',
    activities: activities,
  ),
  Destination(
    imageUrl: 'assets/images/anfa.jfif',
    city: 'Anfa',
    country: 'Casablanca',
    description: 'Touvez votre service tout près de chez vous.',
    activities: activities,
  ),
];

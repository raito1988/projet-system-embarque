class freelancer {
  final String name;
  final double rating;
  final String role;
  final int jobsdone;
  final String sal;
  final String imgpath;

  freelancer({
    this.name,
    this.rating,
    this.jobsdone,
    this.role,
    this.sal,
    this.imgpath,
  });

  static List<freelancer> freelancer1 = [
    freelancer(
      name: "Anass",
      jobsdone: 6,
      rating: 4.5,
      role: "Musculation",
      imgpath: "anafilali.jfif",
      sal: "06 11 22 11 22",
    ),
    freelancer(
        name: "Amine",
        jobsdone: 16,
        rating: 4.0,
        imgpath: "ali.jfif",
        role: "Musculation",
        sal: "06 66 66 66 66"),
    freelancer(
        name: "Imane",
        jobsdone: 18,
        rating: 3.5,
        imgpath: "f1.jpeg",
        role: "Musculation ",
        sal: "06 55 55 55 55"),
    freelancer(
        name: "Halim",
        jobsdone: 2,
        rating: 4.1,
        imgpath: "f1.jpeg",
        role: "coding1",
        sal: "06 11 11 11 11"),
    freelancer(
        name: "lamiae",
        jobsdone: 6,
        rating: 4.5,
        imgpath: "f1.jpeg",
        role: "coding",
        sal: "06 99 99 99 99"),
    freelancer(
        name: "Saoussan",
        jobsdone: 6,
        rating: 4.5,
        imgpath: "f1.jpeg",
        role: "Musculation",
        sal: "06 88 88 88 88"),
    freelancer(
        name: "Ali",
        jobsdone: 6,
        rating: 4.5,
        imgpath: "f1.jpeg",
        role: "Musculation",
        sal: "06 77 77 77 77"),
    freelancer(
        name: "Afaf",
        jobsdone: 6,
        rating: 4.5,
        imgpath: "f1.jpeg",
        role: "Musculation",
        sal: "06 76 76 76 76"),
    freelancer(
        name: "Amer",
        jobsdone: 6,
        rating: 4.5,
        imgpath: "f1.jpeg",
        role: "Musculation",
        sal: "06 43 43 43 43"),
  ];
}

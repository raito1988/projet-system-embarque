class menageprofond {
  final String name;
  final String role;
  final int jobsdone;
  final String tel;
  final String imgpath;

  menageprofond({
    this.name,
    this.jobsdone,
    this.role,
    this.tel,
    this.imgpath,
  });

  static List<menageprofond> freelancer1 = [
    menageprofond(
      name: "Aboubakr Alaoui",
      jobsdone: 6,
      role: "Menage Profond",
      imgpath: "f1.jpeg",
      tel: "06 62 43 43 43 ",
    ),
    menageprofond(
      name: "Ilyass Histake",
      jobsdone: 6,
      role: "Menage Profond",
      imgpath: "f1.jpeg",
      tel: "06 62 43 11 43 ",
    ),
    menageprofond(
      name: "Imane Benmansour",
      jobsdone: 6,
      role: "Menage Profond",
      imgpath: "f1.jpeg",
      tel: "06 62 43 45 43 ",
    ),
    menageprofond(
      name: "Alaoui Mharzi Aymane",
      jobsdone: 6,
      role: "Menage Profond",
      imgpath: "f1.jpeg",
      tel: "06 63 34 45 43 ",
    ),
    menageprofond(
      name: "Hanane Issaoui",
      jobsdone: 6,
      role: "Menage Profond",
      imgpath: "f1.jpeg",
      tel: "06 61 98 98 43 ",
    ),
  ],
      freelancer2 = [
        menageprofond(
          name: "Aboubakr Alaoui",
          jobsdone: 6,
          role: "Menage Profond",
          imgpath: "f1.jpeg",
          tel: "06 62 43 43 43 ",
        ),
        menageprofond(
          name: "Ilyass Histake",
          jobsdone: 6,
          role: "Menage Profond",
          imgpath: "f1.jpeg",
          tel: "06 62 43 11 43 ",
        ),
        menageprofond(
          name: "Imane Benmansour",
          jobsdone: 6,
          role: "Menage Profond",
          imgpath: "f1.jpeg",
          tel: "06 62 43 45 43 ",
        ),
        menageprofond(
          name: "Alaoui Mharzi Aymane",
          jobsdone: 6,
          role: "Menage Profond",
          imgpath: "f1.jpeg",
          tel: "06 63 34 45 43 ",
        ),
        menageprofond(
          name: "Hanane Issaoui",
          jobsdone: 6,
          role: "Menage Profond",
          imgpath: "f1.jpeg",
          tel: "06 61 98 98 43 ",
        ),
      ];
}

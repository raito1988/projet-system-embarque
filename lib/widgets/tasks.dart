import 'package:bifadden/models/task.dart';
import 'package:bifadden/screens/Garde/garde.dart';
import 'package:bifadden/screens/chef/chef.dart';
import 'package:bifadden/screens/coach/coach.dart';
import 'package:bifadden/screens/proff/proff.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class taskcard extends StatefulWidget {
  taskcard({Key key}) : super(key: key);

  @override
  _taskcardState createState() => _taskcardState();
}

class _taskcardState extends State<taskcard> {
  List<task> tasklist = task.taskk;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.symmetric(horizontal: 3),
      itemCount: tasklist.length,
      itemBuilder: (context, index) {
        return Container(
          color: Colors.transparent,
          width: MediaQuery.of(context).size.width * 0.30,
          child: Card(
            child: InkWell(
              splashColor: Colors.black,
              onTap: () {
                if (tasklist[index].taskname == "Coach") {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => coachroom(),
                    ),
                  );
                }
                if (tasklist[index].taskname == "Professeur") {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => proffroom(),
                    ),
                  );
                }
                if (tasklist[index].taskname == "Chef") {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => chefroom(),
                    ),
                  );
                }
                if (tasklist[index].taskname == "Garde") {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => garderoom(),
                    ),
                  );
                }
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(
                        "assets/images/${tasklist[index].bgimgpath}"),
                  ),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        top:15.0,
                        bottom: 10.0,
                      ),
                      child: Container(
                        height: 70,
                        width: 80,
                        child: SvgPicture.asset(
                            "assets/svg/${tasklist[index].imgpath}"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 14.0, right: 14.0),
                      child: Text("${tasklist[index].taskname}"),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

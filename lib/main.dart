import 'package:bifadden/screens/home.dart';
import 'package:bifadden/screens/home_certifie/profile_screen.dart';
import 'package:bifadden/screens/ville/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'Pages/LoginPage.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bricollage',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: "oswald",
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentindex = 0;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List tabs = [
      Home(),
      HomeScreen(),
      CertifieScreen(),
      LoginScreen(),
    ];
    return SafeArea(
      child: Scaffold(
        body: tabs[_currentindex],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentindex,
          elevation: 5,
          mouseCursor: MouseCursor.defer,
          backgroundColor: Colors.white54,
          selectedItemColor: Colors.red[800],
          type: BottomNavigationBarType.fixed,
          selectedIconTheme: IconThemeData(size: 30),
          selectedFontSize: 15,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              title: Text("Accueil"),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.map_rounded,
              ),
              title: Text("Ville"),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.whatshot,
              ),
              title: Text("Certifié"),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.account_box_rounded,
              ),
              title: Text("Poster"),
            ),
          ],
          onTap: (index) {
            setState(() {
              _currentindex = index;
            });
          },
        ),

      ),
    );
  }
}

